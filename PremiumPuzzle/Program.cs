
static void ExampleA()
{
    Console.WriteLine("\nExample A");
    Console.WriteLine("~~~~~~~~~");

    Console.WriteLine("If we have a circle with a radius of 4cm, what is its area?");
    string piSymbol = "π";                                  //Literal Character. Or could have used unicode \u03A0"
    Console.WriteLine($"Formula is 2 {piSymbol} r");

    float radius = 4;
    float pi = 3.1416f;
    //double pi = Math.PI;                                  // Alternative is to use Math module.
    float area = pi * radius * radius;

    // You need to stop using ASCII and use Unicode to get the squared symbol
    Console.OutputEncoding = System.Text.Encoding.Unicode;
    Console.WriteLine($"Answer: {area} cm\u00B2");          // "\u00B2" is the squared symbol in unicode
}


// Puzzle A - More Maths
// 
// Solve these maths problems
/*
static void PuzzleA()
{
    Console.WriteLine("\nPuzzle A");
    Console.WriteLine("~~~~~~~~~");

    Console.WriteLine("A circle has a diameter of 100m. What is the circumference?");

    Console.WriteLine("\nFind the hypotenuse (longest side) of a right-angled triangle, if the other 2 sides are 5cm long and 12cm long?");
    
}
*/


static void ExampleB()
{
    Console.WriteLine("\nExample B");
    Console.WriteLine("~~~~~~~~~");

    // A US dollar is worth 1.49 Dollerydoos
    // As we are not using decimal.Parse() on a user's input, we need to specify the number is a decimal
    // The 'm' in 1.49m will do this for us. Otherwise, in the background C# will actually process it as a double.
    decimal exchangeRate = 1.49m;

    Console.WriteLine($"Current USD to AUD exchange rate: ${exchangeRate}");

    decimal usdMoney = 100m;
    decimal ausMoney = usdMoney * exchangeRate;

    Console.WriteLine($"{usdMoney} USD is approximately {ausMoney} AUD.");
    Console.WriteLine($"That's a lot of dollerydoos!");
    Console.WriteLine($"That'll get me a carton of piss (24 cans of beer) for sure bro!");

}


// Puzzle B - AmsterDAMNN!
//
// An American tourist is going to Amsterdam.
// Marijuana joints cost 30 Euros.
// A pint of Heineken beer costs 15 Euros.
// The American tourist wants to spend $500 USD "getting wasted".
// Write a program that calculates the maximum beers or joints they could buy
// Look up the USD to Euro exchange rate
// Remember to ROUND DOWN. If you can only afford 0.666 of a drink they won't sell it to you!
/*
static void PuzzleB()
{
    Console.WriteLine("\nPuzzle B");
    Console.WriteLine("~~~~~~~~~");
}
*/


static void ExampleC()
{
    Console.WriteLine("\nExample C");
    Console.WriteLine("~~~~~~~~~");

    Console.WriteLine("How many Donald Trumps does it take to screw in a lightbulb?");

    Random randy = new Random();
    int donalds = randy.Next(1, 100);

    Console.WriteLine($"Maybe {donalds} ?");

    Console.WriteLine("No. Just one, but he’ll claim it was the best lightbulb change in history!");

}


// Puzzle C - Art of the deal
//
// Donald Trump wants to automate deal making!
// Write a program for him that asks the user to enter their best offer
// "Enter your best offer for this deal in USD (fantastic currency, the best!): "
// Then just counter-offer between 10% and 50% of that amount.
// Don't expect to get paid for writing this program....

// Don't worry about a user entering something that may crash the program
// We have not done if statements or error checking yet
/*
static void PuzzleC()
{
    Console.WriteLine("\nPuzzle C");
    Console.WriteLine("~~~~~~~~~");
}
*/


// Run the puzzles

ExampleA();
//PuzzleA();

ExampleB();
//PuzzleB();

ExampleC();
//PuzzleC();



Console.WriteLine("\n Press enter to exit the program...");
Console.ReadLine();                                         // Keeps the console app window open until you hit enter